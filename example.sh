#!/bin/bash

# what is the name of the tmux session
SESSION_NAME=test

# what windows do we need in that session
WINDOW_NAMES=(abc def ghi jkl)

# argument to the script, e.g 'stop' to close session
COMMAND=$1

source ~/.local/share/tmux-functions/tmux-functions.sh

case "$COMMAND" in
    "stop")
        tmux kill-session -t $SESSION_NAME
        ;;

    *)
        start_session_and_open_windows $SESSION_NAME "${WINDOW_NAMES[@]}"
        tmux attach -t $SESSION_NAME
        ;;
esac
