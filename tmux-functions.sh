start_session_and_open_windows() {
    local SESSION_NAME=$1
    shift
    local WINDOW_NAMES=("$@")

    if [ -n "$TMUX" ]; then
        echo "Can not run inside another tmux session"
        exit
    fi

    if tmux has-session -t=$SESSION_NAME 2>/dev/null; then
        tmux attach -t $SESSION_NAME
        exit;
    fi

    tmux new-session -d -s $SESSION_NAME -n ${WINDOW_NAMES[0]}

    for i in ${!WINDOW_NAMES[@]}; do
        if [ $i -eq 0 ]; then
            continue
        fi

        local WINDOW_NAME=${WINDOW_NAMES[$i]}

        tmux new-window -n $WINDOW_NAME
    done
}

