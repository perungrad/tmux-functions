# Tmux Functions

Functions to help with project setup

## Installation

Clone the repo into `~/.local/share/tmux-functions` directory.

```bash
cd ~/.local/share
git clone git@bitbucket.org:perungrad/tmux-functions.git
```

## Usage

Include `tmux-functions.sh` into your script and use the imported functions 😄

```bash
source ~/.local/share/tmux-functions/tmux-functions.sh

start_session_and_open_windows my-session window-1 window-2 window-3
tmux attach -t my-session
```

See `example.sh` for example.
